package com.example.copategaadmin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class CupActivity extends AppCompatActivity {

    private TextInputLayout titleWrapper;
    private TextInputLayout yearWrapper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cup);
    }

    public void onClickBtn(View v){

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        titleWrapper = findViewById(R.id.title);
        yearWrapper = findViewById(R.id.year);

        String title = titleWrapper.getEditText().getText().toString();
        String year = yearWrapper.getEditText().getText().toString();

        Map<String, Object> cup = new HashMap<>();
        cup.put("nome", title);
        cup.put("ano", year);

        db.collection("copas")
                .add(cup)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d("DB", "DocumentSnapshot added with ID: "
                                + documentReference.getId());

                        Toast.makeText(getApplicationContext(),
                                "Copa Criada com Sucesso!",
                                Toast.LENGTH_LONG).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("DB", "Error adding document", e);

                        Toast.makeText(getApplicationContext(),
                                "Ops!! Algo deu errado. Tente novamente mais tarde.",
                                Toast.LENGTH_LONG).show();
                    }
                });
    }
}
